CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

USE mywebsite;

create table user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
login_passwprd varchar(255) NOT NULL,
name varchar(255) NOT NULL,
birthday date NOT NULL,
address varchar(255) NOT NULL,
create_date date NOT NULL,
update_date date NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



create table delivery_method(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
name varchar(256) NOT NULL,
price int NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



create table buy(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
total_price int NOT NULL,
create_date datetime NOT NULL,
delivery_id int NOT NULL,
user_id int NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


create table item(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
name varchar(256) NOT NULL,
detail text  NOT NULL,
price int NOT NULL,
category varchar(256) NOT NULL,
file_name varchar(256) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



create table buy_detail(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
buy_pieces int NOT NULL,
subtotal_price int NOT NULL,
buy_id int NOT NULL,
item_id int NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO delivery_method (`id`, `name`, `price`) VALUES
(1, '特急配送', 500),
(2, '通常配送', 0),
(3, '日時指定配送', 200);



INSERT INTO item ( `name`, `detail`, `price`, `category`, `file_name`) VALUES
( 'ディズニー ツイステッドワンダーランド 飛行術アクリルダイカットキーホルダーVol.2','ツイステッドワンダーランドから可愛くデフォルメされた【飛行術アクリルダイカットキーホルダー】が登場！第二弾はオクタヴィネル寮とスカラビア寮とイグニハイド寮の全7種類になります！お気に入りの子をGETしよう！', 660,'キーホルダー','../twst/resize_image.jpg');
